import sys #only for MacOS
sys.path.append('/usr/local/Cellar/opencv/2.4.13.2/lib/python2.7/site-packages')
import cv2
import numpy as np
import math

img = cv2.imread('prova.jpg')
img = cv2.resize(img, (640, 480))
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
gray = cv2.medianBlur(gray, 15)
edges = cv2.Canny(gray, 35, 125)
#edges = cv2.Canny(gray,100,200,apertureSize = 3)

minLineLength = 30
maxLineGap = 10
lines = cv2.HoughLinesP(edges,1,np.pi/180,15,minLineLength,maxLineGap)
for x in range(0, len(lines)):
    for x1,y1,x2,y2 in lines[x]:
        cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)

lines = cv2.HoughLines(edges, 1, np.pi/180, 200)
for rho,theta in lines[0]:
    print(round(math.degrees(theta)))

cv2.imshow('hough',img)
cv2.waitKey(0)
