import threading
from threading import Thread
from time import sleep

CLOUD_LIVE = False
c = threading.Condition()

def funzione():
    global CLOUD_LIVE
    while True:
        c.acquire()
        if CLOUD_LIVE:
            print(CLOUD_LIVE)
            CLOUD_LIVE = False
        else:
            print(CLOUD_LIVE)
            CLOUD_LIVE = True
        c.release()
        sleep(1)


if __name__ == "__main__":
    global CLOUD_LIVE
    thread = Thread(target = funzione, args = [])
    thread.start()
    while True:
        c.acquire()
        print("Il valore di cloud e':")
        print(CLOUD_LIVE)
        c.release()
        sleep(1)
