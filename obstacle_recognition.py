# import the necessary packages
import numpy as np
#import sys #only for MacOS
#sys.path.append('/usr/local/Cellar/opencv/2.4.13.2/lib/python2.7/site-packages')
import cv2
import pika
import requests
import math
import io
import threading
import time
from threading import Thread
from time import sleep
import os

MIN_MATCH_COUNT = 15 #previous 30

def find_angle(img):
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray = cv2.medianBlur(gray, 15)
    edges = cv2.Canny(gray, 35, 125)

    lines = cv2.HoughLines(edges, 1, np.pi/180, 120)
    try:
        rho,theta = lines[0][0]
        if(round(math.degrees(theta))>87 and round(math.degrees(theta)<93)):
            print("OK")
            print(round(math.degrees(theta)))
    except:
        print("Unable to detect angle")

def send_to_cloud(image, channel, cap):
    try:
        cv2.imwrite("prova.jpg", image)
        with open('prova.jpg', 'rb') as f: r = requests.post('https://advancedvideorecognition.sloppy.zone', f, timeout=0.3)
        if (r.text.find("warning")>=0):
            channel.basic_publish(exchange='obstacle', routing_key='', body='cloud')
            return 3
        elif(r.text.find("detect")>=0):
            channel.basic_publish(exchange='obstacle', routing_key='', body='cloud')
            return 1 # "pericolo generico"
        else:
            channel.basic_publish(exchange='obstacle', routing_key='', body='no_cloud')
            return 1
    except:
        channel.basic_publish(exchange='obstacle', routing_key='', body='no_cloud')
        return 1 # "pericolo generico"

def find_end(image):
    res, array = cv2.findChessboardCorners(image, (3, 3), None, flags=cv2.CALIB_CB_FAST_CHECK+cv2.CALIB_CB_FILTER_QUADS)

    return res

def obstacle_detector(QueryImgBGR):
    #initialization
    detector = cv2.SIFT()
    FLANN_INDEX_KDITREE = 0
    flannParam = dict(algorithm = FLANN_INDEX_KDITREE, tree = 5)
    flann = cv2.FlannBasedMatcher(flannParam, {})

    trainImg = cv2.imread("example.jpg", 0)
    trainKP,trainDesc = detector.detectAndCompute(trainImg, None)


    QueryImg = cv2.cvtColor(QueryImgBGR,cv2.COLOR_BGR2GRAY)
    queryKP,queryDesc = detector.detectAndCompute(QueryImg,None)
    matches = flann.knnMatch(queryDesc,trainDesc,k=2)

    goodMatch = []
    for m,n in matches:
        if(m.distance < 0.75*n.distance):
            goodMatch.append(m)
    if(len(goodMatch) > MIN_MATCH_COUNT):
        print("Found!")
        return 2
    else:
        print "Not Enough match found- %d/%d"%(len(goodMatch), MIN_MATCH_COUNT)
        return 0

def find_redobject(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

	# find the colors within the specified boundaries
    mask = cv2.inRange(hsv, lower, upper)

    maskOpen=cv2.morphologyEx(mask,cv2.MORPH_OPEN,kernelOpen)
    maskClose=cv2.morphologyEx(maskOpen,cv2.MORPH_CLOSE,kernelClose)
    maskFinal=maskClose
    #conts,h=cv2.findContours(maskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    (_, cnts, _)=cv2.findContours(maskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)

    if(maskFinal.any()==False):
        return 0 # no red detected

    return 1 # RED found

def find_obstacle(image, channel, cap):
    # convert the image to grayscale, blur it, and detect edges
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #gray = cv2.GaussianBlur(gray, (5, 5), 0)
    gray = cv2.medianBlur(gray, 15)
    edged = cv2.Canny(gray, 35, 125)

    # find the contours in the edged image and keep the largest one;
    (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    #(cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    try:
        cont = max(cnts, key = cv2.contourArea)
    except:
        return 0

    ###### obstacle detected! #####

    # check if there is end_line
    if(find_end(image)):
        return 2    #end_line detected

    # check if there is a red object
    if(find_redobject(image)):
        # check if the cloud is active
        return send_to_cloud(image, channel, cap)
    return 0


if __name__ == "__main__":
    # create NumPy arrays from the boundaries
    lower=np.array([0,150,150])
    upper=np.array([50,255,255])

    #morphology
    kernelOpen=np.ones((5,5))
    kernelClose=np.ones((20,20))

    #setup RabbitMQ connection
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='10.10.11.33', socket_timeout=10000))
    #connection = pika.BlockingConnection(pika.ConnectionParameters(host='194.168.1.42', socket_timeout=10000))
    #connection = pika.BlockingConnection(pika.ConnectionParameters(host='194.168.1.100', socket_timeout=10000))
    channel = connection.channel()

    channel.exchange_declare(exchange='obstacle', exchange_type='fanout')

    flag = 0
    while True:
        cap = cv2.VideoCapture("http://194.168.1.8:8080")
        #cap = cv2.VideoCapture("http://192.168.1.91:8080")
        #important to avoid heartbeat deadlock
        connection.process_data_events()
        if(cap.isOpened()):
            print("Connected")
            channel.basic_publish(exchange='obstacle', routing_key='', body='clean')
        else:
            print("Webcam not ready")
            sleep(1)
        while(cap.isOpened()):
            ret, frame = cap.read()
            if ret:
                frame = cv2.resize(frame, (320, 240))
                new = find_obstacle(frame, channel, cap)
                if(flag!=new):
                    flag=new
                    if(flag==0):
                        channel.basic_publish(exchange='obstacle', routing_key='', body='clean')
                        print("Sent: clean")

                    elif(flag==1):
                        channel.basic_publish(exchange='obstacle', routing_key='', body='detect')
                        print("Sent: detect")

                    elif(flag==2):
                        channel.basic_publish(exchange='obstacle', routing_key='', body='end_line')
                        print("Sent: end_line")
                        sleep(2)

                    else:
                        channel.basic_publish(exchange='obstacle', routing_key='', body='warning')
                        print("Sent: warning")

                    cap.read()
                    cap.read()
            else:
                print("Disconnected")
                break
        cap.release()
    connection.close()
    print("Exit")
    os._exit(0)
