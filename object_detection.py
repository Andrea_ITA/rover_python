import sys #only for MacOS
sys.path.append('/usr/local/Cellar/opencv/2.4.13.2/lib/python2.7/site-packages')
import cv2
import numpy as np
import pika
MIN_MATCH_COUNT=18 #previous 30

detector=cv2.SIFT()

FLANN_INDEX_KDITREE=0
flannParam=dict(algorithm=FLANN_INDEX_KDITREE,tree=5)
flann=cv2.FlannBasedMatcher(flannParam,{})

trainImg=cv2.imread("example.jpg",0)
trainKP,trainDesc=detector.detectAndCompute(trainImg,None)

cam= cv2.VideoCapture("http://192.168.1.91:8080")
while True:
    while cam.isOpened():
        ret, QueryImgBGR=cam.read()
        QueryImg=cv2.cvtColor(QueryImgBGR,cv2.COLOR_BGR2GRAY)
        queryKP,queryDesc=detector.detectAndCompute(QueryImg,None)
        matches=flann.knnMatch(queryDesc,trainDesc,k=2)

        goodMatch=[]
        for m,n in matches:
            if(m.distance<0.75*n.distance):
                goodMatch.append(m)
        if(len(goodMatch)>MIN_MATCH_COUNT):
            print("Found!")
        else:
            print "Not Enough match found- %d/%d"%(len(goodMatch),MIN_MATCH_COUNT)
        #cv2.imshow('result',QueryImgBGR)
        if cv2.waitKey(10)==ord('q'):
            break
    print("Cam not ready")
    time.sleep(2)
cam.release()
