FROM valian/docker-python-opencv-ffmpeg
MAINTAINER Andrea Alfo' <s232100@studenti.polito.it>
ADD obstacle_recognition.py /
RUN pip install pika
RUN pip install requests
CMD ["python", "-u", "./obstacle_recognition.py"]
