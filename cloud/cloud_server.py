#!/usr/bin/env python
"""
Very simple HTTP server in python.
Usage::
    ./dummy-web-server.py [<port>]
Send a GET request::
    curl http://localhost
Send a HEAD request::
    curl -I http://localhost
Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost
"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import numpy as np
#import sys #only for MacOS
#sys.path.append('/usr/local/Cellar/opencv/2.4.13.2/lib/python2.7/site-packages')
import cv2
from os import curdir
from os.path import join as pjoin

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write("<html><body><h1>hi!</h1></body></html>")

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        self.store_path = pjoin(curdir, 'download.jpg')
        content_length = int(self.headers['Content-Length'])
        data = self.rfile.read(content_length)

        with open(self.store_path, 'wb') as fh:
            fh.write(data)

        self._set_headers()
        self.wfile.write(find_redobject(cv2.imread('download.jpg')))

def run(server_class=HTTPServer, handler_class=S, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

def find_redobject(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

	# find the colors within the specified boundaries
    mask = cv2.inRange(hsv, lower, upper)

    maskOpen=cv2.morphologyEx(mask,cv2.MORPH_OPEN,kernelOpen)
    maskClose=cv2.morphologyEx(maskOpen,cv2.MORPH_CLOSE,kernelClose)
    maskFinal=maskClose
    #conts,h=cv2.findContours(maskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    (_, cnts, _) = cv2.findContours(maskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)

    if(maskFinal.any()==False):
        return find_yellowobject(hsv)

    print("Obstacle detected!")
    return "detect"

def find_yellowobject(hsv):
    # find the colors within the specified boundaries
    mask = cv2.inRange(hsv, lowerY, upperY)

    maskOpen=cv2.morphologyEx(mask,cv2.MORPH_OPEN,kernelOpen)
    maskClose=cv2.morphologyEx(maskOpen,cv2.MORPH_CLOSE,kernelClose)
    maskFinal=maskClose
    #conts,h=cv2.findContours(maskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    (_, cnts, _) = cv2.findContours(maskFinal.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)


    if(maskFinal.any()==False):
        print("No obstacle detected")
        return "clean"

    print("Yellow obstacle detected")
    return "warning"

if __name__ == "__main__":
    from sys import argv

    # create NumPy arrays from the boundaries
    lower=np.array([0,150,150])
    upper=np.array([22,255,255])

    lowerY=np.array([22,150,150])
    upperY=np.array([50,255,255])

    #morphology
    kernelOpen=np.ones((5,5))
    kernelClose=np.ones((20,20))

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
