import sys #only for MacOS
sys.path.append('/usr/local/Cellar/opencv/2.4.13.2/lib/python2.7/site-packages')
import cv2
import numpy as np
import math

def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)

	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper)

	# return the edged image
	return edged

img = cv2.imread('prova.jpg', 0)
edges = auto_canny(img)

lines = cv2.HoughLines(edges, 1, np.pi/180, 200)
try:
    for rho,theta in lines[0]:
        print(round(math.degrees(theta)))
except:
    print("Error")
